#include <gtk/gtk.h>
#include <time.h>
#include <stdio.h>

GtkWidget *drawing_area;
GtkWidget *window;
GtkWidget *label; // Declare label globally

gboolean on_mouse_move(GtkWidget *widget, GdkEventMotion *event, gpointer user_data) {
    gtk_window_move(GTK_WINDOW(window), (gint)event->x_root, (gint)event->y_root);
    return TRUE;
}

gboolean update_time(gpointer user_data) {
    GtkWidget *label = GTK_WIDGET(user_data); // Cast user_data back to GtkWidget
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

  strftime(buffer, sizeof(buffer), "%I:%M %p %a %d", timeinfo); // Time first, then date
    gtk_label_set_text(GTK_LABEL(label), buffer);

    return G_SOURCE_CONTINUE;
}

gboolean follow_mouse(gpointer user_data) {
    GdkDisplay *display = gdk_display_get_default();
    GdkSeat *seat = gdk_display_get_default_seat(display);
    GdkDevice *pointer = gdk_seat_get_pointer(seat);
    gint x, y;
    gdk_device_get_position(pointer, NULL, &x, &y);

    // Change the offsets here to adjust the window position relative to the cursor
    gtk_window_move(GTK_WINDOW(window), x + 5, y + 20); // Offset to position window 5px right and 20px below the cursor

    return G_SOURCE_CONTINUE;
}

gboolean draw_callback(GtkWidget *widget, cairo_t *cr, gpointer user_data) {
    GdkPixbuf *pixbuf = user_data;
    gdk_cairo_set_source_pixbuf(cr, pixbuf, 0, 0);
    cairo_paint_with_alpha(cr, 0.53); // Set image opacity to 0.5
    return FALSE;
}

int main(int argc, char *argv[]) {
    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Time Display");
    gtk_window_set_default_size(GTK_WINDOW(window), 33, 33); // Set the window size
    gtk_window_set_decorated(GTK_WINDOW(window), FALSE); // Remove window borders
    gtk_window_set_keep_above(GTK_WINDOW(window), TRUE); // Keep the window above others
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    // Set the window to be transparent
    GdkScreen *screen = gtk_widget_get_screen(window);
    GdkVisual *visual = gdk_screen_get_rgba_visual(screen);
    if (visual != NULL && gdk_screen_is_composited(screen)) {
        gtk_widget_set_visual(window, visual);
    }
    gtk_widget_set_app_paintable(window, TRUE);
    gtk_widget_override_background_color(window, GTK_STATE_FLAG_NORMAL, &(GdkRGBA){0, 0, 0, 0}); // Set window background color to transparent

    // Disable all interactions with the window
    gtk_widget_set_sensitive(window, FALSE);

    // Create a fixed container to overlay label on image
    GtkWidget *fixed = gtk_fixed_new();
    gtk_container_add(GTK_CONTAINER(window), fixed);

    // Load the image
    GError *error = NULL;
    GdkPixbuf *pixbuf_orig = gdk_pixbuf_new_from_file("me.png", &error);
    if (error) {
        g_printerr("Error loading image: %s\n", error->message);
        g_error_free(error);
        return 1;
    }

    // Scale the image to the desired width and height
    GdkPixbuf *pixbuf_scaled = gdk_pixbuf_scale_simple(pixbuf_orig, 32.89, 32.89, GDK_INTERP_BILINEAR);

    drawing_area = gtk_drawing_area_new();
    gtk_widget_set_size_request(drawing_area, 32.89, 32.89); // Set the size of the drawing area
    gtk_container_add(GTK_CONTAINER(fixed), drawing_area);

    g_signal_connect(drawing_area, "draw", G_CALLBACK(draw_callback), pixbuf_scaled);

    // Initialize and style the label
    label = gtk_label_new(NULL);
    gtk_widget_override_font(label, pango_font_description_from_string("Sans 14")); // Set font size to 15px

    // Set text color to white with opacity 0.85
    GdkRGBA text_color;
    gdk_rgba_parse(&text_color, "rgba(255, 255, 255, 0.94)"); // White color with 85% opacity
    gtk_widget_override_color(label, GTK_STATE_FLAG_NORMAL, &text_color);

    gtk_fixed_put(GTK_FIXED(fixed), label, 0, 0); // Place label on top of the image

    gtk_widget_show_all(window);

    g_timeout_add_seconds(1, update_time, label); // Pass label to update_time

    // Update the window position to follow the mouse every 10ms
    g_timeout_add(10, follow_mouse, NULL);

    gtk_main();

    // Clean up
    g_object_unref(pixbuf_orig);
    g_object_unref(pixbuf_scaled);

    return 0;
}
