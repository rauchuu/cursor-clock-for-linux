Compile the C program name it 'my_program'
and make sure their is a 'me.png'  on the same directory
so you can use whatever picture you want just make sure its named 'me.png' it must be a png file i believe for the transparency to working im not too sure to be honest my first C program

You should be able to run it by double clicking on my_program


#Configurating on Startup
# Autostart my_program with me.png Dependency

This guide explains how to set up `my_program` to run at startup and ensure it can access `me.png` in the user's home directory.

## Prerequisites

- Ensure `my_program` is executable.
- Ensure `me.png` is accessible in the home directory.

## Steps

###-1. Ensure my_program is Executable

Make sure `my_program` is executable by running the following command:

```sh
chmod +x ~/my_program


###---2. Create a Script to Launch my_program

#Create a new script to launch my_program and ensure it can access me.png.
nano ~/launch_my_program.sh

###-------3. Make the Script Executable

Make the script executable by running:

chmod +x ~/launch_my_program.sh

##---4  Create the .desktop file for autostart: 
[Desktop Entry]
Type=Application 
Name=Launch My Program 
Exec=/home/rauchuu/launch_my_program.sh ##--wherever directory your compiled program is 
Icon=/home/rauchuu/Pictures/albert ##-- i dont have an icon idk tf this do
Comment=Launches my_program at startup ##the comment 
X-GNOME-Autostart-enabled=true  ##--NOW THIS IS VERY IMPORTANT I WENT ON A WILD GOOSE CHASE TRYING TO FIND THIS 

##---5 final. Move the .desktop File to the Autostart Directory

mv ~/Desktop/launch_my_program.desktop ~/.config/autostart/




